import { Component, OnInit } from '@angular/core';
import { AutenticacaoService } from '../services/autenticacao.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public validaForm: FormGroup;

  constructor(private autenticacaoService: AutenticacaoService, private formBuilder: FormBuilder) {
    //recupera os dados do formulário e valida se os campos estão preenchidos
    this.validaForm = this.formBuilder.group({
      email: ['', Validators.required],
      senha: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  /**
   * método responsável por chamar o serviço que irá fazer a autenticação do usuário 
   */
  login(){
    this.autenticacaoService.login(this.validaForm.value);
  }
}
