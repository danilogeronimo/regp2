import { Component } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import * as moment from 'moment';
import { Platform } from '@ionic/angular';
import { ToastService } from '../services/toast/toast.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

const KEY = "CONFIGS";

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  notificacao;
  lembretes;

  constructor(private localNotification: LocalNotifications,
    private platform: Platform,
    private ToastService: ToastService,
    private backgroundMode: BackgroundMode,
    public alertController: AlertController,
    private storage: Storage) {
    this.platform.ready()
      .then(() => {
        //seta as configurações default do background mode e localNotification
        this.backgroundMode.setDefaults({
          silent: true
        });
        this.localNotification.setDefaults({
          led: {
            color: '#00d5ff',
            on: 500,
            off: 500
          },
          vibrate: true
        });

        //seta as preferências salvas pelo usuário na página de configurações
        this.storage.get('CONFIGS')
        .then(configs=>{ 
          this.lembretes = configs.lembretes;
          this.notificacao = configs.notificacao;
        })

      })
  }

  /**
   * exibe um modal contendo informações
   * @param entidade entidade que está invocando o botão de mostrar ajuda
   */
  async mostraAjuda(entidade) {
    let msg = '';
    if (entidade == 1) {
      msg = 'Ao ativar essa opção, será disparado uma notificação ao usuário todos os dias às 8h e às 17h.';
    } else {
      msg = 'Ao escolher uma das opções será mostrado um lembrete no tempo selecionado';
    }

    const alert = await this.alertController.create({
      header: 'Ajuda',
      subHeader: 'Ativar Notificação',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  /**
   * método responsável por setar as configurações dos lembretes e notificações e também por salva-lás no local storage
   */
  configuraLembrete() {

    let configs = {
      lembretes : this.lembretes,
      notificacao: this.notificacao
    }

    //salva as preferencias no local storage
    this.storage.set(KEY, configs);

    if (this.lembretes !== undefined && this.lembretes !== "") {
      let lembrete = moment(new Date()).add(this.lembretes, 'minutes').toDate();
      //solicita permissão do sistema para exibir a notificação
      this.localNotification.requestPermission();

      //agenda a notificação
      this.localNotification.schedule([
        {
          id: 3,
          title: 'Olá! ',
          text: 'Atenção! Isso é um lembrete',
          trigger: { at: new Date(new Date(lembrete).getTime()) },
          led: { color: '#FF00FF', on: 500, off: 500 },
          vibrate: true,
          foreground: true
        }
      ]);
      //verifica se a notificação foi agendada e exibe uma mensagem
      this.localNotification.isScheduled(3)
        .then(() => {
          this.ToastService.showToast('Lembrete agendado para ' + moment(new Date(new Date(lembrete).getTime())).format("HH:mm"));
        })
        .catch(err => this.ToastService.showToast(err))
    } else {
      //desativa a notificação e desativa o background mode
      this.localNotification.cancel(3);
      this.backgroundMode.setEnabled(false);
      this.ToastService.showToast('Notificação desativada');
    }

    if (this.notificacao !== undefined && this.notificacao !== false) {
      this.ativaNoficacao(this.notificacao);
    } else {
      //desativa as notificações e desativa o background mode
      this.localNotification.cancel(1);
      this.localNotification.cancel(2);
      this.backgroundMode.setEnabled(false);
      this.ToastService.showToast('Notificação desativada');
    }

  }
  /**
   * Ativa as notificações de entrada e saída, utilizando o momentjs + local notification.
   * @param status //status do elemento da view (ativado e desativado)
   */
  ativaNoficacao(status) {
    if (status) {
      //solicita permissão do sistema para exibir a notificação
      this.localNotification.requestPermission();

      //agenda as notificações
      this.localNotification.schedule([
        {
          id: 1,
          title: 'Olá ',
          text: 'Atenção! Não se esqueça de registrar o ponto!',
          trigger: {every: { hour: 8, minute: 0 } },
          foreground: true
        },
        {
          id: 2,
          title: 'segundo ',
          text: 'Atenção! Não se esqueça de registrar o ponto!',
          trigger: { every: { hour: 17, minute: 0 } },
          foreground: true
        }
      ]);

      //verifica se a notificação foi agendada e exibe uma mensagem
      this.localNotification.isScheduled(1)
        .then(() => {
          this.ToastService.showToast('Lembretes Agendados!');
        });
    }
  }

}
