import { Component } from '@angular/core';
import { PontoDaoService } from '../services/ponto/ponto-dao.service';
import * as moment from 'moment';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  pontos;
  momentjs: any = moment;

  constructor(private pontoDao: PontoDaoService) {
    this.listaPontos('');
  }

  /**
   * método invocado ao iniciar página e pelo ionic refresher para exibir a lista de pontos na tela
   * @param event evento enviado pelo ionic refresher
   */
  async listaPontos(event) {
    setTimeout(()=>{
      this.pontoDao.lista()
        .then(pontos => {
          this.pontos = pontos;
        });

        if(event !== undefined || event !== ""){
          event.target.complete();
        }
    },1500)

  }


}
