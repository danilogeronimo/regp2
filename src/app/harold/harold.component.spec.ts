import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HaroldComponent } from './harold.component';

describe('HaroldComponent', () => {
  let component: HaroldComponent;
  let fixture: ComponentFixture<HaroldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HaroldComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HaroldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
