import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-harold',
  templateUrl: './harold.component.html',
  styleUrls: ['./harold.component.scss'],
})
export class HaroldComponent implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  naoMeClica(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
