import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { AutenticacaoService } from '../services/autenticacao.service';
import { NetworkService } from '../services/network.service';
import { ToastService } from '../services/toast/toast.service';
import { PontoService } from '../services/ponto/ponto.service';
import { ViewController } from '@ionic/core';

@Component({
  selector: 'app-home-popover',
  templateUrl: './home-popover.component.html',
  styleUrls: ['./home-popover.component.scss'],
})
export class HomePopoverComponent implements OnInit {

  constructor(public popoverCtrl: PopoverController,
    private autenticacaoService: AutenticacaoService,
    private network: NetworkService,
    private pontoService: PontoService,
    private toast: ToastService) { }

  ngOnInit() { }

  close() {
    this.popoverCtrl.dismiss();
  }

  /**
   * realizar logout do usuário
   */
  logout() {
    this.autenticacaoService.logout();
    this.popoverCtrl.dismiss();
  }

  /**
   * sincroniza dados com a API
   */
  sincroniza() {
    if (this.network.verificaConexao()) {
      this.pontoService.sincronizaDados();
    } else {
      this.toast.showToast("Você precisa estar conectado para sincronizar! Tente novamente.")
    }
  }

}
