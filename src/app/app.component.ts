import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { PowerManagement } from '@ionic-native/power-management/ngx';
import { AutenticacaoService } from './services/autenticacao.service';
import { Router } from '@angular/router';
declare var cordova;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private backgroundMode: BackgroundMode,
    private powerManagement: PowerManagement,
    private AutenticacaoService: AutenticacaoService,
    private router: Router,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      //ativa o background mode para que o app continue rodando em segundo plano para exibir as notificações e o powermanager para evitar que o android encerre o app
      this.backgroundMode.enable();
      cordova.plugins.backgroundMode.enable();
      this.backgroundMode.on('activate').subscribe(
        () => this.backgroundMode.disableWebViewOptimizations()
      );
      cordova.plugins.backgroundMode.on('activate',function(){
        cordova.plugins.backgroundMode.disableWebViewOptimizations();
      });
      cordova.plugins.backgroundMode.setDefaults({ silent: true });
      this.powerManagement.acquire();

      //o subscribe fica ouvindo o behavior subject q inicialmente possui o valor false
      this.AutenticacaoService.authState.subscribe(state => {
        if (state) {
          this.router.navigate(['tabs']);
        } else {
          this.router.navigate(['login']);
        }
      });
    });
  }
}
