import { Injectable } from '@angular/core';
import { Ponto } from 'src/app/modelos/ponto';
import { Storage } from '@ionic/storage';

const KEY = "ponto";

@Injectable({
  providedIn: 'root'
})
export class PontoDaoService {

  constructor(private storage: Storage) { }

  /**
   * método responsável por gravar o registro do ponto no local storage
   * @param ponto dados do registro do ponto a ser gravado
   */
  grava(ponto: Ponto): Promise<any> {
    return this.storage.get(KEY)
      .then((pontos: Ponto[]) => {
        if (pontos) { //se houver pontos salvos, adiciona o novo na array de pontos existentes
          pontos.push(ponto);
          return this.storage.set(KEY, pontos);
        } else {
          return this.storage.set(KEY, [ponto]); //senao retorna uma array com o ponto novo
        }
      });
  }

  /**
   * lista os pontos gravados no local storage
   */
  async lista() {
    return await this.storage.get(KEY);
  }

  /**
   * método responsável por gravar os pontos que foram enviados pela API pelo método de sincronização
   * @param pontos dados do registro de pontos que foram enviados pela API
   */
  gravaPontosSincronizados(pontos):Promise<any>{
    this.storage.remove(KEY); //remove os registros antigos e grava novamente
    return this.storage.set(KEY,pontos);
  }

}
