import { TestBed } from '@angular/core/testing';

import { PontoDaoService } from './ponto-dao.service';

describe('PontoDaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PontoDaoService = TestBed.get(PontoDaoService);
    expect(service).toBeTruthy();
  });
});
