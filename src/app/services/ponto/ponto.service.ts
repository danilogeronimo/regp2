import { Injectable } from '@angular/core';
import { Ponto } from 'src/app/modelos/ponto';
import { PontoDaoService } from './ponto-dao.service';
import { PontoApiService } from '../API/ponto-api.service';
import { ToastService } from '../toast/toast.service';
import { Vibration } from '@ionic-native/vibration/ngx';


@Injectable({
  providedIn: 'root'
})
export class PontoService {

  constructor(private pontoDao: PontoDaoService,
    private pontoApi: PontoApiService,
    private toast: ToastService,
    private vibration: Vibration,
  ) { }

  /**
   * método do serviço responsável por tratar a inserção dos dados dos registros de pontos
   * @param ponto dados do registro de ponto a serem gravados
   */
  cadastra(ponto: Ponto) {
    return new Promise((resolve, reject) => {
      this.pontoDao.grava(this.trocaTipo(ponto))
        .then(ponto => {
          resolve("Ponto registrado com sucesso!");
        })
        .catch((err: Error) => {
          reject(err);
        });
    });
  }

  /**
   * Recebe o ponto verifica o tipo do último ponto e faz a troca (1)entrada (2)saída
   * @param ponto 
   */
  trocaTipo(ponto: Ponto) {
    this.pontoDao.lista()
      .then(data => {
        if (data)
          ponto.tipo = data[data.length - 1].tipo == 1 ? 2 : 1;
      })
    return ponto;
  }

  /**
   * método do serviço responsável por invocar o método que retorna a lista dos pontos cadastrados
   */
  async lista() {
    return await this.pontoDao.lista();
  }

  /**
   * método responsável por gerar a lista de dados que serão sincronizados pela API
   * @param force flag para 'forçar' a sincronização, ou seja, enviará todos os dados contidos no local storage
   */
  listaASincronizar(force = false) {
    let aSincronizar = [];

    return new Promise((resolve, reject) => {
      this.pontoDao.lista()
        .then(dados => {
          if (dados !== null && dados['status'] !== "fail") {
            dados.forEach(element => {
              //sincronização forçada. Envia todos os dados
              if (force) {
                aSincronizar.push(element);
              } else {
                if (!element.sync) {
                  aSincronizar.push(element);
                }
              }
            });
            resolve(aSincronizar);
          } else {
            resolve([]);
          }
        }).catch(erro => reject(erro));
    });
  }

  /**
   * método chamado pelo botão sincronizar. Gera uma lista com os dados a serem sincronizados, envia os dados para a API e depois grava os novos dados recebidos no localstorage
   */
  sincronizaDados() {
    this.vibration.vibrate(300);
    this.listaASincronizar(true)
      .then(dados => {
        //faz o post na api      
        this.pontoApi.sincronizaApi(dados)
          .then(result => {
            if (result !== undefined && result !== "") {
              this.pontoDao.gravaPontosSincronizados(result)
                .then(() => {
                  this.toast.showToast("Dados sincronizados com sucesso!");
                })
                .catch(erro => {
                  console.log(erro);
                  this.toast.showToast("Erro ao salvar dados a sincronziados")
                });
            }
          }).catch(erro => {
            this.toast.showToast('Erro ao sincronizar com a API');
            console.log(erro);
            this.vibration.vibrate([100, 50, 100]);
          });
      })
      .catch(erro => {
        console.log(erro)
        this.toast.showToast("Erro ao gerar a lista para sincronizar.");
      });
  }
}
