import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Usuario } from '../modelos/usuario';
import { NetworkService } from './network.service';
import { ToastService } from './toast/toast.service';
import { PontoApiService } from './API/ponto-api.service';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { LoaderService } from './loader.service';

const KEY = "USUARIO";

@Injectable({
  providedIn: 'root'
})
export class AutenticacaoService {

  //seta o valor inicial de autenticado como false
  authState = new BehaviorSubject(false);

  constructor(private router: Router,
    private storage: Storage,
    private platform: Platform,
    public toast: ToastService,
    private network: NetworkService,
    private apiService: PontoApiService,
    public http: HttpClient,
    private navCtrl: NavController,
    public loader: LoaderService) {
    this.platform.ready().then(() => {
      this.logado();
    });
  }

  /**
   * método responsável por verificar se o usuário já está logado no sistema
   */
  logado() {
    this.storage.get(KEY).then((response) => {
      if (response) {
        this.authState.next(true); // Ao logar é passado para o subject de autenticação o valor true
      }
    });
  }


  /**
   * método do serviço responsável por logar o usuário
   * @param usuario 
   */
  login(usuario) {
    var user: Usuario = {
      uuid: '',
      sync: false,
      dados: {
        nome: 'danilo',
        email: 'danilo.geronimo@hotmail.com',
        senha: usuario.senha
      }
    };
    
    if (!this.network.verificaConexao()) {
      this.toast.showToast("Você precisa estar conectado para fazer o login!");
    } else {
      //conecta na API para validar usuario
      this.autenticaUsuario(user);
    }
  }

  /**
   * método responsável por conectar na API e autenticar o usuário que está tentando realizar o login
   * @param user usuário para ser autenticado
   */
  autenticaUsuario(user) {
    this.loader.exibeLoader('Realizando o Login. Por favor, aguarde...');
    this.http.post(this.apiService.retornaUrlApi + 'usuarios/autentica', user)
      .subscribe(
        () => {
          //se autenticou na API é gravada as informações do usuário logado no local storage e a rota redireciona para a página principal
          this.storage.set(KEY, user).then(() => {
            this.loader.fechaLoader();
            this.authState.next(true); //ao logar é passado para o subject de autenticação o valor true
            this.navCtrl.navigateRoot('tabs');
          }).catch(err=>{
            console.log(err);
            this.loader.fechaLoader();
          });
        },
        (erro) => {
          console.log(JSON.stringify(erro))
          this.loader.fechaLoader();
          this.toast.showToast(JSON.stringify(erro['error']['msg']))
        }
      );
  }

  /**
   * realiza o logout do usuário, remove a configuração do localstorage e redireciona para a tela de login com a autenticação setada como false
   */
  logout() {
    this.storage.remove(KEY).then(() => {
      this.authState.next(false); //ao deslogar é passado para o subject o valor false
      this.router.navigate(['login']);
    });
  }

  //retorna se o usuário está autenticado (estado do subject)
  autenticado() {
    return this.authState.value;
  }

  //retorna usuario logado
  usuarioLogado(){
    return this.storage.get(KEY);
  }

}
