import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor() {}

  //verifica o estado da conexão
  verificaConexao(){
    if(!navigator.onLine){
      return false;
    }
    return true;
  }
  
}
