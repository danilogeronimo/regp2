import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  loader: any;

  constructor(public loadingController: LoadingController) { }

  exibeLoader(msg) {
    this.loader = this.loadingController.create({
      message: msg
    }).then((res) => {
      res.present();
    });
    
  }
  
  fechaLoader() {
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 1000);
  }
}

