import { TestBed } from '@angular/core/testing';

import { PontoApiService } from './ponto-api.service';

describe('PontoApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PontoApiService = TestBed.get(PontoApiService);
    expect(service).toBeTruthy();
  });
});
