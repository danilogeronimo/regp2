import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoaderService } from '../loader.service';

@Injectable({
  providedIn: 'root'
})
export class PontoApiService {

  // private url: string = "http://10.10.10.5:3000/";
  private url: string = "https://regpws.herokuapp.com/";
  loaderToShow: any;

  constructor(public http: HttpClient, public loader: LoaderService) { }

  /**
   * retorna a url da api
   */
  get retornaUrlApi() {
    return this.url;
  }

  /**
   * método que irá fazer a sincronização dos dados do local storage com a API
   * @param dados dados que serão enviados a API para sincronizar
   */
  sincronizaApi(dados) {
    return new Promise((resolve, reject) => {
      let headers = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      };

      if (dados !== "") {
        this.loader.exibeLoader("Sincronizando dados. Por favor, aguarde...");
        this.http.post(this.url + 'sincroniza', JSON.stringify(dados), headers)
          .subscribe(
            (res) => {
              this.loader.fechaLoader();
              resolve(res);
            },
            (err) => {
              this.loader.fechaLoader();
              reject(err)
            }
          );
      }
    });
  }  
}
