import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController) { }

  /**
   * Exibe uma mensagem na tela com as configurações abaixo
   * @param msg string contendo a mensagem a ser exibida
   */
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'top',
      duration: 3000
    });
    toast.present();
  }
}

