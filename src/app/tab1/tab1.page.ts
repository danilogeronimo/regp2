import { Component } from '@angular/core';
import { Ponto } from '../modelos/ponto';
import { PontoService } from '../services/ponto/ponto.service';
import { AutenticacaoService } from '../services/autenticacao.service';
import { PopoverController, ModalController } from '@ionic/angular';
import { HomePopoverComponent } from '../home-popover/home-popover.component';
import { ToastService } from '../services/toast/toast.service';
import { Md5 } from 'ts-md5/dist/md5';
import format from 'date-fns/format';
import { Vibration } from '@ionic-native/vibration/ngx';
import { HaroldComponent } from '../harold/harold.component';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  usuario: any;
  harold: number = 0;

  constructor(private pontoService: PontoService, 
    private autenticacaoService: AutenticacaoService,
    public popoverCtrl: PopoverController,
    private toastService: ToastService,
    private vibration: Vibration,
    private modalCtrl: ModalController) {
      //recupera o usuário logado para exibir na tela
      this.autenticacaoService.usuarioLogado()
    .then(dados=>{
      this.usuario = dados['dados'].nome;
    });
    }


  /**
   * método responsável por registrar os pontos do usuário
   */
  registraPonto() {
    this.vibration.vibrate(300);

    //verifica se o usuário está logado
    this.autenticacaoService.usuarioLogado()
      .then(dados=>{

        this.usuario = dados['dados'].nome;

        let ponto: Ponto = {
          uuid: this.geraId(),
          sync:0,          
          usuario: 1,
          tipo: 1,
          motivo: '',
          modificado: 0,
          data_ponto: format(new Date(), 'yyyy/MM/dd HH:mm:ss')
          
        }
    
        //cadastra o ponto
        this.pontoService.cadastra(ponto)
          .then((msg)=>{
            this.toastService.showToast(msg);
          })
          .catch(erro=>{
            console.log(erro);
            this.toastService.showToast('Erro ao gravar o ponto');
            this.vibration.vibrate([100,50,100]);
          });
        
      }).catch((erro)=>{
        console.log(erro);
        this.toastService.showToast("O usuário não está logado!")
      });
  }

  /**
   * exibe o menu de sincronização e logout
   */
  async abreMenu(event){
    const popover = await this.popoverCtrl.create({
      component: HomePopoverComponent,
      event:event,
      translucent: true
    });
    return await popover.present();    
  }

  /**
   * gera uma uuid para o usuário para ser usada na sincronização
   */
  private geraId(): any {
    let time = new Date();
    let hash = Md5.hashStr(time.toString());
    return hash;
  }















  async meClica(){
    this.harold++;
    if(this.harold == 5){
      const modal = await this.modalCtrl.create({
        component: HaroldComponent
      });
      modal.present();
      this.harold = 0;
    }
  }
}
