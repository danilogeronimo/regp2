import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { HomePopoverComponent } from '../home-popover/home-popover.component';
import { HaroldComponent } from '../harold/harold.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab1Page }])
  ],
  declarations: [Tab1Page, HomePopoverComponent, HaroldComponent],
  entryComponents:[HomePopoverComponent, HaroldComponent]
})
export class Tab1PageModule {}
